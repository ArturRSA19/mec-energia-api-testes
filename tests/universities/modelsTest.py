import unittest
from universities import ConsumerUnit

class TestConsumerUnit(unittest.TestCase):
    def setUp(self):
        self.consumer_unit = ConsumerUnit()

    def test_get_energy_bills_by_year_valid(self):
        year = 2022  
        result = self.consumer_unit.get_energy_bills_by_year(year)
        self.assertIsInstance(result, list)  

    def test_get_energy_bills_by_year_invalid(self):
        year = 3000  
        with self.assertRaises(Exception):
            self.consumer_unit.get_energy_bills_by_year(year)






